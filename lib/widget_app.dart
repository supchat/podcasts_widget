import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:podcasts_widget/empty_session/empty_session_screen.dart';
import 'package:podcasts_widget/live_session_screen/live_session_screen.dart';
import 'package:podcasts_widget/main/main_bloc.dart';
import 'package:podcasts_widget/repository/session.dart';
import 'package:podcasts_widget/repository/session_repository.dart';
import 'package:podcasts_widget/sl/get_it.dart';
import 'package:podcasts_widget/upcoming_session/upcoming_session_screen.dart';

class WidgetApp extends StatelessWidget {
  final Color primaryColor = Colors.green;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiBlocProvider(
        providers: [
          BlocProvider<MainBloc>(
            create: (context) =>
                MainBloc(getIt<SessionRepository>())..checkCurrentSession(),
          ),
        ],
        child: Scaffold(body: _buildInitialScreen()),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

  Widget _buildInitialScreen() {
    return MultiBlocListener(
      listeners: [
        BlocListener<MainBloc, MainState>(
          listener: (context, state) {
            if (state is SessionEmptyState) {
              _navigateToEmptySession(context);
            } else if (state is SessionUpcomingState) {
              _navigateToUpcomingSession(context, state.session);
            } else if (state is SessionLiveState) {
              _navigateToLiveSession(context, state.session);
            }
          },
        ),
      ],
      child: BlocBuilder<MainBloc, MainState>(builder: (_, state) {
        return Stack(
          children: <Widget>[if (state is MainInitial) _buildLoading()],
        );
      }),
    );
  }

  void _navigateToEmptySession(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EmptySessionScreen()),
    );
  }

  void _navigateToUpcomingSession(BuildContext context, Session session) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UpcomingSessionScreen(session: session),
      ),
    );
  }

  void _navigateToLiveSession(BuildContext context, Session session) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LiveSessionScreen(session: session),
      ),
    );
  }

  Widget _buildLoading() {
    return Positioned(
      child: Container(
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
          ),
        ),
        color: Colors.white.withOpacity(0.8),
      ),
    );
  }
}
