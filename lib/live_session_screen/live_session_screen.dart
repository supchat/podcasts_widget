import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:podcasts_widget/broadcast_screen/broadcast_participant.dart';
import 'package:podcasts_widget/broadcast_screen/broadcast_screen.dart';
import 'package:podcasts_widget/live_session_screen/bloc/join_session_bloc.dart';
import 'package:podcasts_widget/repository/js_interface.dart';
import 'package:podcasts_widget/repository/session.dart';
import 'package:podcasts_widget/repository/session_repository.dart';
import 'package:podcasts_widget/sl/get_it.dart';

class LiveSessionScreen extends StatelessWidget {
  final Session session;

  LiveSessionScreen({Key key, this.session}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => JoinSessionBloc(
        getIt<JsInterface>(),
        getIt<SessionRepository>(),
      ),
      child: Scaffold(
        body: Container(
          color: Colors.black87,
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "🔴 LIVE",
                    style: TextStyle(color: Colors.red, fontSize: 24),
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
              Text(
                session.topic,
                style: const TextStyle(color: Colors.white, fontSize: 24),
              ),
              const SizedBox(height: 10.0),
              Text(
                "Broadcast started at ${session.startTime}",
                style: const TextStyle(color: Colors.white, fontSize: 20),
              ),
              const SizedBox(height: 10.0),
              Text(
                "Host: ${session.host.name}",
                style: const TextStyle(color: Colors.white, fontSize: 20),
              ),
              const SizedBox(height: 10.0),
              Text(
                "Listeners: ${session.listeners}",
                style: const TextStyle(color: Colors.white, fontSize: 14),
              ),
              const SizedBox(height: 16.0),
              BlocListener<JoinSessionBloc, JoinSessionState>(
                listener: (context, state) {
                  if (state is JoinedSessionState) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BroadcastScreen(
                          broadcast: state.broadcast,
                          participant: BroadcastParticipant(
                              id: state.id, name: state.id),
                        ),
                      ),
                    );
                  }
                },
                child: Builder(
                  builder: (context) => FlatButton(
                    onPressed: () {
                      BlocProvider.of<JoinSessionBloc>(context).joinSession();
                    },
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.blue,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(24.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: const Text(
                        "Join broadcast",
                        style: TextStyle(color: Colors.blue, fontSize: 24),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
