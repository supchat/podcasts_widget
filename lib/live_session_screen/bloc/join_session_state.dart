part of 'join_session_bloc.dart';

@immutable
abstract class JoinSessionState {}

class JoinSessionInitial extends JoinSessionState {}

class JoiningSessionState extends JoinSessionState {
  final Broadcast broadcast;

  JoiningSessionState(this.broadcast);
}

class JoinedSessionState extends JoinSessionState {
  final String id;
  final Broadcast broadcast;

  JoinedSessionState(this.id, this.broadcast);
}
