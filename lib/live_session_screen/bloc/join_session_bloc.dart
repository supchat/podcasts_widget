import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:podcasts_widget/repository/broadcast.dart';
import 'package:podcasts_widget/repository/js_interface.dart';
import 'package:podcasts_widget/repository/session_repository.dart';

part 'join_session_state.dart';

class JoinSessionBloc extends Cubit<JoinSessionState> {
  final JsInterface jsInterface;
  final SessionRepository sessionRepository;

  JoinSessionBloc(
    this.jsInterface,
    this.sessionRepository,
  ) : super(JoinSessionInitial()) {
    jsInterface.addOnJoinedListener(this, (id) async {
      final broadcast = (state as JoiningSessionState).broadcast;
      emit(JoinedSessionState(id, broadcast));
    });
  }

  Future<void> joinSession() async {
    final broadcast = await sessionRepository.joinSession();
    print("Token: ${broadcast.voiceSessionId}");
    final participantId = DateTime.now().microsecondsSinceEpoch.toString();
    emit(JoiningSessionState(broadcast));
    jsInterface.joinSessionJs(broadcast.voiceSessionId, participantId);
  }

  @override
  Future<void> close() {
    jsInterface.removeOnJoinedListener(this);
    return super.close();
  }
}
