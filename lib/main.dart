import 'package:flutter/material.dart';
import 'package:podcasts_widget/logging.dart';
import 'package:podcasts_widget/repository/js_layer.dart';
import 'package:podcasts_widget/sl/get_it.dart';
import 'package:podcasts_widget/widget_app.dart';

void main() {
  initDependencies();
  initBlocLogging();
  initJsInterop();
  runApp(WidgetApp());
}
