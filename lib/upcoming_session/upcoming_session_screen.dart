import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:podcasts_widget/repository/session.dart';

class UpcomingSessionScreen extends StatelessWidget {
  final Session session;

  const UpcomingSessionScreen({Key key, this.session}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black87,
        width: double.maxFinite,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              session.topic,
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
            const SizedBox(height: 10.0),
            Text(
              "Broadcast will start at ${session.startTime}",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            const SizedBox(height: 10.0),
            Text(
              "Host: ${session.host.name}",
              style: TextStyle(color: Colors.white, fontSize: 20),
            )
          ],
        ),
      ),
    );
  }
}
