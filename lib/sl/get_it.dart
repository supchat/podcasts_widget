import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:podcasts_widget/repository/js_interface.dart';
import 'package:podcasts_widget/repository/session_repository.dart';

final getIt = GetIt.instance;

initDependencies() {
  getIt.registerLazySingleton<JsInterface>(() => JsInterface());
  getIt.registerLazySingleton<Logger>(() => Logger());
  getIt.registerLazySingleton<SessionRepository>(() => SessionRepository());
}
