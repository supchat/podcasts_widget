import 'package:podcasts_widget/repository/participant.dart';

class Host extends Participant{
  Host(String id, String name, String photoUrl) : super(id, name, photoUrl);
}
