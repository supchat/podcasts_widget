import 'package:podcasts_widget/repository/audience.dart';
import 'package:podcasts_widget/repository/session_status.dart';
import 'package:podcasts_widget/repository/stage_participant.dart';

class Broadcast {
  final String sessionId;
  final String voiceSessionId;
  final SessionStatus status;
  final DateTime startTime;
  final String topic;
  final StageParticipant host;
  final List<StageParticipant> stage;
  final Audience audience;

  Broadcast(
    this.sessionId,
    this.voiceSessionId,
    this.status,
    this.startTime,
    this.topic,
    this.host,
    this.stage,
    this.audience,
  );
}
