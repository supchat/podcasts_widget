@JS()
library callable_function;

import 'package:js/js.dart';
import 'package:podcasts_widget/repository/js_interface.dart';
import 'package:podcasts_widget/sl/get_it.dart';

final jsInterface = getIt<JsInterface>();

@JS('joinSession')
external String joinSession(String token, String participantId);

@JS('leaveSession')
external String leaveSession();

@JS('onJoined')
external set _onJoined(void Function(String) function);

@JS('onParticipantJoined')
external set _onParticipantJoined(void Function(String) function);

@JS('onParticipantLeft')
external set _onParticipantLeft(void Function(String) function);

@JS('onParticipantStartedSpeaking')
external set _onParticipantStartedSpeaking(void Function(String) function);

@JS('onParticipantStoppedSpeaking')
external set _onParticipantStoppedSpeaking(void Function(String) function);

initJsInterop() {
  _onJoined = allowInterop(_onJoinedProxy);
  _onParticipantJoined = allowInterop(_onParticipantJoinedProxy);
  _onParticipantLeft = allowInterop(_onParticipantLeftProxy);
  _onParticipantStartedSpeaking =
      allowInterop(_onParticipantStartedSpeakingProxy);
  _onParticipantStoppedSpeaking =
      allowInterop(_onParticipantStoppedSpeakingProxy);
}

void _onJoinedProxy(String id) {
  print("_onJoinedProxy");
  jsInterface.notifyOnJoined(id);
}

void _onParticipantJoinedProxy(String id) {
  print("_onParticipantJoinedProxy");
  jsInterface.notifyOnParticipantJoinedListeners(id);
}

void _onParticipantLeftProxy(String id) {
  print("_onParticipantLeftProxy");
  jsInterface.notifyOnParticipantLeftListeners(id);
}

void _onParticipantStartedSpeakingProxy(String id) {
  print("_onParticipantStartedSpeakingProxy");
  jsInterface.notifyOnParticipantStartedSpeakingListeners(id);
}

void _onParticipantStoppedSpeakingProxy(String id) {
  print("_onParticipantStoppedSpeakingProxy");
  jsInterface.notifyOnParticipantStoppedSpeakingListeners(id);
}
