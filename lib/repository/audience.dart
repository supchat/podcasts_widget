import 'package:podcasts_widget/repository/stage_request.dart';

class Audience {
  final int listeners;
  final List<StageRequest> stageRequests;

  Audience(
    this.listeners,
    this.stageRequests,
  );
}
