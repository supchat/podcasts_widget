import 'package:podcasts_widget/repository/participant.dart';

class StageParticipant extends Participant {
  final bool isMuted;

  StageParticipant(
    id,
    name,
    photoUrl,
    this.isMuted,
  ) : super(id, name, photoUrl);
}
