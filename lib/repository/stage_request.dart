import 'package:podcasts_widget/repository/participant.dart';

class StageRequest extends Participant {
  final String message;

  StageRequest(
    id,
    name,
    photoUrl,
    this.message,
  ) : super(id, name, photoUrl);
}
