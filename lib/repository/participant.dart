class Participant {
  final String id;
  final String name;
  final String photoUrl;

  Participant(
    this.id,
    this.name,
    this.photoUrl,
  );
}
