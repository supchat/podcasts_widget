import 'package:podcasts_widget/repository/host.dart';
import 'package:podcasts_widget/repository/session_status.dart';

class Session {
  final String sessionId;
  final SessionStatus status;
  final DateTime startTime;
  final String topic;
  final int listeners;
  final Host host;

  Session(
    this.sessionId,
    this.status,
    this.startTime,
    this.topic,
    this.listeners,
    this.host,
  );
}
