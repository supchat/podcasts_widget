import 'package:podcasts_widget/repository/js_layer.dart';

class JsInterface {
  Map<dynamic, Function(String)> onJoinedListeners = {};
  Map<dynamic, Function(String)> onParticipantJoinedListeners = {};
  Map<dynamic, Function(String)> onParticipantLeftListeners = {};
  Map<dynamic, Function(String)> onParticipantStartedSpeakingListeners = {};
  Map<dynamic, Function(String)> onParticipantStoppedSpeakingListeners = {};

  void joinSessionJs(String token, String participantId) {
    joinSession(token, participantId);
  }

  void leaveSessionJs() {
    leaveSession();
  }

  void notifyOnJoined(String id) {
    onJoinedListeners.forEach((key, listener) {
      listener(id);
    });
  }

  void addOnJoinedListener(dynamic key, Function function) {
    onJoinedListeners[key] = function;
  }

  void removeOnJoinedListener(dynamic key) {
    final a = onJoinedListeners.remove(key);
    print("=====///=====> $a");
  }

  void notifyOnParticipantJoinedListeners(String id) {
    onParticipantJoinedListeners.forEach((key, listener) {
      listener(id);
    });
  }

  void addOnParticipantJoinedListener(dynamic key, Function function) {
    onParticipantJoinedListeners[key] = function;
  }

  void removeOnParticipantJoinedListener(dynamic key) {
    onParticipantJoinedListeners.remove(key);
  }

  void notifyOnParticipantLeftListeners(String id) {
    onParticipantLeftListeners.forEach((key, listener) {
      listener(id);
    });
  }

  void addOnParticipantLeftListener(dynamic key, Function function) {
    onParticipantLeftListeners[key] = function;
  }

  void removeOnParticipantLeftListener(dynamic key) {
    onParticipantLeftListeners.remove(key);
  }

  void notifyOnParticipantStartedSpeakingListeners(String id) {
    onParticipantStartedSpeakingListeners.forEach((key, listener) {
      listener(id);
    });
  }

  void addOnParticipantStartedSpeakingListeners(dynamic key, Function function) {
    onParticipantStartedSpeakingListeners[key] = function;
  }

  void removeOnParticipantStartedSpeakingListeners(dynamic key) {
    onParticipantStartedSpeakingListeners.remove(key);
  }

  void notifyOnParticipantStoppedSpeakingListeners(String id) {
    onParticipantStoppedSpeakingListeners.forEach((key, listener) {
      listener(id);
    });
  }

  void addOnParticipantStoppedSpeakingListeners(dynamic key, Function function) {
    onParticipantStoppedSpeakingListeners[key] = function;
  }

  void removeOnParticipantStoppedSpeakingListeners(dynamic key) {
    onParticipantStoppedSpeakingListeners.remove(key);
  }
}
