import 'package:dio/dio.dart';
import 'package:podcasts_widget/repository/audience.dart';
import 'package:podcasts_widget/repository/broadcast.dart';
import 'package:podcasts_widget/repository/host.dart';
import 'package:podcasts_widget/repository/session.dart';
import 'package:podcasts_widget/repository/session_status.dart';
import 'package:podcasts_widget/repository/stage_participant.dart';

class SessionRepository {
  final _dio = Dio();

  Future<Session> getSessionStatus() async {
    return Session(
      "sessionId",
      SessionStatus.live,
      DateTime.now(),
      "Test session (always live)",
      0,
      Host("id111", "John Doe", null),
    );
  }

  Future<Broadcast> joinSession() async {
    final userId = DateTime.now().toIso8601String();
    final sessionId = 'main_session';

    Response<Map> response = await _dio.post(
      'https://supchat.ai/widget/sessions/get-token',
      data: {'sessionName': sessionId, 'userId': userId},
      options: Options(headers: {'Content-type': 'application/json'}),
    );

    return Broadcast(
      "sessionId",
      response.data['token'],
      SessionStatus.live,
      DateTime.now(),
      "Test session (always live)",
      StageParticipant("id222", "John Doe", null, false),
      [],
      Audience(0, []),
    );
  }
}
