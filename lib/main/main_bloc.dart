import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:podcasts_widget/repository/session.dart';
import 'package:podcasts_widget/repository/session_repository.dart';
import 'package:podcasts_widget/repository/session_status.dart';

part 'main_state.dart';

class MainBloc extends Cubit<MainState> {
  final SessionRepository _sessionRepository;

  MainBloc(this._sessionRepository) : super(MainInitial());

  Future<void> checkCurrentSession() async {
    final currentSession = await _sessionRepository.getSessionStatus();
    if (currentSession.status == SessionStatus.live) {
      emit(SessionLiveState(currentSession));
    } else if (currentSession.status == SessionStatus.upcoming) {
      emit(SessionUpcomingState(currentSession));
    } else if (currentSession.status == SessionStatus.empty) {
      emit(SessionEmptyState());
    }
  }
}
