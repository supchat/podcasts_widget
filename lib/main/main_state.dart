part of 'main_bloc.dart';

@immutable
abstract class MainState {}

class MainInitial extends MainState {}

class SessionLiveState extends MainState {
  final Session session;

  SessionLiveState(this.session);
}

class SessionUpcomingState extends MainState {
  final Session session;

  SessionUpcomingState(this.session);
}

class SessionEmptyState extends MainState {}
