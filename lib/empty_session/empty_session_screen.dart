import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class EmptySessionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black87,
        child: Center(
          child: const Text(
            "No broadcast right now, stay tuned!",
            style: TextStyle(color: Colors.white, fontSize: 24),
          ),
        ),
      ),
    );
  }
}
