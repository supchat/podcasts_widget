import 'package:bloc/bloc.dart';
import 'package:logger/logger.dart';
import 'package:podcasts_widget/sl/get_it.dart';

initBlocLogging() {
  Bloc.observer = AppBlocObserver(getIt<Logger>());
}

class AppBlocObserver extends BlocObserver {

  final Logger _logger;

  AppBlocObserver(this._logger);

  @override
  void onCreate(BlocBase cubit) {
    super.onCreate(cubit);
    _logger.d('onCreate -- cubit: ${cubit.runtimeType}');
  }

  @override
  void onChange(BlocBase cubit, Change change) {
    super.onChange(cubit, change);
    final text = 'onChange -- cubit: ${cubit.runtimeType}, change: $change';
    _logger.d(text);
  }

  @override
  void onError(BlocBase cubit, Object error, StackTrace stackTrace) {
    final text = 'onError -- cubit: ${cubit.runtimeType}, error: $error';
    _logger.d(text);
    super.onError(cubit, error, stackTrace);
  }

  @override
  void onClose(BlocBase cubit) {
    super.onClose(cubit);
    final text = 'onClose -- cubit: ${cubit.runtimeType}';
    _logger.d(text);
  }
}
