part of 'broadcast_bloc.dart';

@immutable
abstract class BroadcastState {}

class BroadcastInitial extends BroadcastState {}

class ParticipantsListState extends BroadcastState {
  final List<BroadcastParticipant> participants;

  ParticipantsListState(this.participants);
}

class BroadcastLeft extends BroadcastState {}
