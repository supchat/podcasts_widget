import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:podcasts_widget/broadcast_screen/broadcast_participant.dart';
import 'package:podcasts_widget/repository/js_interface.dart';
import 'package:podcasts_widget/repository/session_repository.dart';

part 'broadcast_state.dart';

class BroadcastBloc extends Cubit<BroadcastState> {
  final JsInterface jsInterface;
  final SessionRepository sessionRepository;

  final List<BroadcastParticipant> _participants = [];

  BroadcastBloc(
    this.jsInterface,
    this.sessionRepository,
  ) : super(BroadcastInitial()) {
    jsInterface.addOnParticipantJoinedListener(this, (id) async {
      print("++> addOnParticipantJoinedListener $id");
      await _addParticipant(BroadcastParticipant(id: id, name: id));
    });
    jsInterface.addOnParticipantLeftListener(this, (id) async {
      print("++> addOnParticipantLeftListener $id");
      await _removeParticipant(id);
    });
    jsInterface.addOnParticipantStartedSpeakingListeners(this, (id) async {
      print("++> addOnParticipantStartedSpeakingListeners $id");
      await _startedSpeakingParticipant(id);
    });
    jsInterface.addOnParticipantStoppedSpeakingListeners(this, (id) async {
      print("++> addOnParticipantStoppedSpeakingListeners $id");
      await _stoppedSpeakingParticipant(id);
    });
    print("++> BroadcastBloc created");
  }

  Future<void> addParticipant(BroadcastParticipant participant) async {
    _addParticipant(participant);
  }

  Future<void> _addParticipant(BroadcastParticipant participant) async {
    _participants.add(participant);
    print("========> ADD $_participants");
    emit(ParticipantsListState(_participants));
  }

  Future<void> _removeParticipant(String id) async {
    _participants.removeWhere((element) => element.id == id);
    print("========> REMOVE $_participants");
    emit(ParticipantsListState(_participants));
  }

  Future<void> _startedSpeakingParticipant(String id) async {
    final participant = _participants.firstWhere((element) => element.id == id);
    participant.isTalking = true;
    print("========> TALKING $_participants");
    emit(ParticipantsListState(_participants));
  }

  Future<void> _stoppedSpeakingParticipant(String id) async {
    final participant = _participants.firstWhere((element) => element.id == id);
    participant.isTalking = false;
    print("========> NOT_TALKING $_participants");
    emit(ParticipantsListState(_participants));
  }

  Future<void> leaveSession() async {
    jsInterface.leaveSessionJs();
    emit(BroadcastLeft());
  }
}
