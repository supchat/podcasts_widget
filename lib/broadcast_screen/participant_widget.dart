import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:podcasts_widget/broadcast_screen/broadcast_participant.dart';

class ParticipantWidget extends StatelessWidget {
  final BroadcastParticipant participant;

  const ParticipantWidget({Key key, this.participant}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {},
          child: Icon(
            participant.isTalking
                ? Icons.volume_up_rounded
                : Icons.account_circle,
            color: Colors.white,
            size: 60.0,
          ),
          style: ElevatedButton.styleFrom(
              shape: CircleBorder(), primary: Colors.green),
        ),
        const SizedBox(height: 8.0),
        Text(participant.name),
      ],
    );
  }
}
