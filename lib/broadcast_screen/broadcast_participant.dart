class BroadcastParticipant {
  String id;
  String name;
  String photoUrl;
  bool isTalking;
  bool isMuted;

  BroadcastParticipant({
    this.id,
    this.name,
    this.photoUrl = "",
    this.isTalking = false,
    this.isMuted = false,
  });

  @override
  String toString() {
    return 'BroadcastParticipant{id: $id, name: $name, photoUrl: $photoUrl, isTalking: $isTalking, isMuted: $isMuted}';
  }
}
