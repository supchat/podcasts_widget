import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:podcasts_widget/broadcast_screen/bloc/broadcast_bloc.dart';
import 'package:podcasts_widget/broadcast_screen/broadcast_participant.dart';
import 'package:podcasts_widget/broadcast_screen/participant_widget.dart';
import 'package:podcasts_widget/repository/broadcast.dart';
import 'package:podcasts_widget/repository/js_interface.dart';
import 'package:podcasts_widget/repository/session_repository.dart';
import 'package:podcasts_widget/sl/get_it.dart';

class BroadcastScreen extends StatelessWidget {
  final Broadcast broadcast;
  final BroadcastParticipant participant;

  final _bloc = BroadcastBloc(
    getIt<JsInterface>(),
    getIt<SessionRepository>(),
  );

  BroadcastScreen({Key key, this.broadcast, this.participant})
      : super(key: key) {
    _bloc.addParticipant(participant);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BroadcastBloc, BroadcastState>(
      bloc: _bloc,
      listener: (context, state) {
        if (state is ParticipantsListState) {
          print("/=/=> ParticipantsListState");
        } else if (state is BroadcastLeft) {
          print("/=/=> BroadcastLeft");
          Navigator.pushNamedAndRemoveUntil(context, "/", (route) => false);
        }
      },
      builder: (context, state) => Scaffold(
        body: Container(
          width: double.maxFinite,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "🔴 LIVE",
                    style: const TextStyle(color: Colors.red, fontSize: 24),
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
              Text(
                broadcast.topic,
                style: const TextStyle(fontSize: 24),
              ),
              const SizedBox(height: 10.0),
              Text(
                "Broadcast started at ${broadcast.startTime}",
                style: const TextStyle(fontSize: 20),
              ),
              const SizedBox(height: 10.0),
              Text(
                "Host: ${broadcast.host.name}",
                style: const TextStyle(fontSize: 20),
              ),
              const SizedBox(height: 10.0),
              Text(
                "Listeners: ${broadcast.audience.listeners}",
                style: const TextStyle(fontSize: 14),
              ),
              const SizedBox(height: 16.0),
              Expanded(child: _buildParticipantsList(state)),
              const SizedBox(height: 16.0),
              FlatButton(
                onPressed: () {
                  _bloc.leaveSession();
                },
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: Colors.blue, width: 1, style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(24.0)),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: const Text(
                    "Leave broadcast",
                    style: TextStyle(color: Colors.blue, fontSize: 24),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildParticipantsList(state) {
    if (state is ParticipantsListState) {
      final participants = state.participants;
      return ListView.builder(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.only(top: 24.0, bottom: 24.0),
        itemBuilder: (context, index) => ParticipantWidget(
          participant: participants[index],
        ),
        itemCount: participants.length,
      );
    } else {
      return Container();
    }
  }
}
